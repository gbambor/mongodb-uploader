const Product = require("../models/product");
const multer = require("multer");

const fileStoreEngine = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "--" + file.originalname);
  },
});

const upload = multer({ storage: fileStoreEngine });

exports.postProducts = (req, res, next) => {
  const name = req.body.name;
  const price = req.body.name;
  const imageUrl = req.body.imageUrl;
};
