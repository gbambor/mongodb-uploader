const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const productSchema = new Schema({
  name: { type: String, required: true },
  price: { type: String, required: true },
  image: { type: String, required: true },
  //   order: mongoose.Types.ObjectId,
});

module.exports = mongoose.model("product", productSchema);
