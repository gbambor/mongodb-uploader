const express = require("express");

const router = express.Router();

const Product = require("../models/product");
const multer = require("multer");

const fileStoreEngine = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "--" + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpeg" ||
    file.mimetype === "image/jpg"
  ) {
    cb(null, true); //acceptss  a file
  } else {
    cb(null, false); //this rejects a file
  }
};

const upload = multer({
  storage: fileStoreEngine,
  limits: {
    fileSize: 1024 * 1024 * 5, //this means 1024 bytes by 1024 bytes which is 1mb and then multiply by 5 is 5mb
  },
  fileFilter: fileFilter,
});

router.post("/prods", upload.single("image"), (req, res) => {
  const file = req.file;
  const name = req.body.name;
  const price = req.body.name;
  const imageUrl = req.file.path;

  const product = new Product({
    name: name,
    price: price,
    image: imageUrl,
  });

  return product
    .save()
    .then((result) =>
      res
        .status(201)
        .json({ mesage: "Product saved Succesfully", product: result })
    );
});

router.get("/prods", (req, res, next) => {
  Product.find()
    // .select("name price _id image")
    // .exec()
    .then((product) => {
      console.log(product);
      res.status(200).json({ result: product, message: "Message Successful" });
    });
});

module.exports = router;
