FROM node:18-alpine

WORKDIR /usr/src/app

COPY package.json package-lock.json ./ # Copy package.json from the host system into the current directory inside the docker image

RUN npm install #Run npm install during the creation of the docker image

RUN npm install -g nodemon

COPY . ./

ENTRYPOINT ["nodemon", "/usr/src/app/server.js"] 

CMD ["npm", "run", "start" ]



