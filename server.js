const mongoose = require("mongoose");
const express = require("express");
const bodyParser = require("body-parser");
const productRoute = require("./routes/product");

const app = express();
// const cors = require("cors");

app.use("/uploads", express.static("uploads"));
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(cors);
app.use(bodyParser.json());

app.use(productRoute);

mongoose
  .connect(
    `mongodb+srv://ikenna:Cornelik@cluster0.kytwgsm.mongodb.net/images?retryWrites=true`
  )
  .then((res) => app.listen(6085));
